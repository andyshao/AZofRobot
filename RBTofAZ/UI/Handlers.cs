﻿using RBTofAZ.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RBTofAZ.UI
{
    class Handlers
    {
        //控制台后台管理已经弃用，仍保留控制台消息循环机制方便拓展.
        public class Status : ICmdHandler
        {
            void ICmdHandler.Handle(string cmd)
            {
                
            }
        }
        public class MessageHistory : ICmdHandler
        {
            void ICmdHandler.Handle(string cmd)
            {
                if (cmd.IndexOf("/msg") == 0)
                {
                    var users = General.mysqlConnection.SelectUser();
                    foreach(var user in users)
                    {
                        PutMessage(user.UserID);
                    }
                }
            }

            private void PutMessage(string id)
            {
                var history = General.mysqlConnection.GetMessageHistoryByID(id);
                if (history.Messages.Count > 0) General.Println("与" + history.UserName + "的聊天记录:");
                foreach (var msg in history.Messages)
                {
                    General.Println("Q:" + msg.Accept);
                    General.Println("A:" + msg.Aply);
                    General.Println("-------------------------------------------------------------\n");
                }
            }
        }
        public class ShowForm : ICmdHandler
        {
            public void Handle(string cmd)
            {
                if (cmd.IndexOf("/frm") == 0)
                {
                    Thread thread = new Thread(delegate ()
                      {
                          new Frm_Manager().ShowDialog();
                      });
                    thread.Start();
                }
            }
        }
    }
}
