﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace RBTofAZ.Net
{
    /// <summary>
    /// 单例HTTP服务器
    /// </summary>
    class HttpServer
    {
        private HttpListener httpListener;
        /// <summary>
        /// 使用制定端口构造服务器
        /// </summary>
        /// <param name="port">侦听http消息的端口</param>
        public HttpServer(int port)
        {
            httpListener = new HttpListener();
            httpListener.Prefixes.Add(string.Format("http://+:{0}/", port));
            //httpListener.Prefixes.Add(string.Format("http://+:{0}/", 443));
        }
        public void Start()
        {
            httpListener.Start();
            httpListener.BeginGetContext(new AsyncCallback(GetContext), httpListener);  
        }
        private static void GetContext(IAsyncResult ar)
        {
            try
            {
                HttpListener httpListener = ar.AsyncState as HttpListener;
                HttpListenerContext context = httpListener.EndGetContext(ar);
                httpListener.BeginGetContext(new AsyncCallback(GetContext), httpListener);
                HttpListenerRequest request = context.Request;
                HttpListenerResponse response = context.Response;
                Dictionary<string, string> Datas = null;
                if (request.HttpMethod == "POST")
                {
                    Datas = HttpRequestHandler.ResolvePOSTdata(request);
                }
                else if (request.HttpMethod == "GET")
                {
                    Datas = HttpRequestHandler.ResolveGETdata(request);
                }
                if (Datas == null) return;
                new DataHandler(response).Handle(Datas);
            }
            catch { }
        }
    }
}
