﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml.Linq;

namespace RBTofAZ.Net
{
    /// <summary>
    /// 处理http请求
    /// </summary>
    class HttpRequestHandler
    {
        /// <summary>
        /// 对GET类型的请求作出处理
        /// </summary>
        /// <param name="request">要处理的请求</param>
        /// <returns>请求中包含的数据</returns>
        public static Dictionary<string, string> ResolveGETdata(HttpListenerRequest request)
        {
            Dictionary<string, string> pairs = new Dictionary<string, string>();
            string[] keys = request.QueryString.AllKeys;
            foreach (var key in keys)
            {
                pairs.Add(key, request.QueryString[key]);
            }
            return pairs;
        }
        /// <summary>
        /// 对POST类型的请求作出处理
        /// </summary>
        /// <param name="request">要处理的请求</param>
        /// <returns>请求中包含的数据</returns>
        public static Dictionary<string, string> ResolvePOSTdata(HttpListenerRequest request)
        {
            Dictionary<string, string> pairs = new Dictionary<string, string>();
            using (Stream stream = request.InputStream)
            {
                StreamReader reader = new StreamReader(stream, Encoding.UTF8);
                string body = reader.ReadToEnd();
                XElement element = XElement.Parse(body);
                foreach (var item in element.DescendantsAndSelf())
                {
                    pairs.Add(item.Name.ToString(), item.Value);
                }
            }
            pairs.Remove(pairs.Keys.First());
            return pairs;
        }
    }
}
