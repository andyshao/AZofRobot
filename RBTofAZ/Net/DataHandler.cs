﻿using RBTofAZ.AI;
using RBTofAZ.AI.Plugins;
using RBTofAZ.AI.Study;
using RBTofAZ.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace RBTofAZ.Net
{
    /// <summary>
    /// 用于对接收到的数据进行处理
    /// </summary>
    class DataHandler
    {
        private HttpListenerResponse ListenerResponse;

        public UserInfo User { get; set; }

        public DataHandler(HttpListenerResponse response)
        {
            ListenerResponse = response;
        }
        /// <summary>
        /// 对发信作出回应
        /// </summary>
        /// <param name="content"></param>
        private void WriteResponse(string content)
        {
            ListenerResponse.ContentType = "html";
            ListenerResponse.ContentEncoding = Encoding.UTF8;
            using (Stream output = ListenerResponse.OutputStream)
            {
                byte[] buffer = Encoding.UTF8.GetBytes(content);
                output.Write(buffer, 0, buffer.Length);
            }
        }
        /// <summary>
        /// 数据处理方法
        /// </summary>
        /// <param name="datas"></param>
        public void Handle(Dictionary<string, string> datas)
        {
            RequestDataType type = RequestDataType.Unknow;
            if (datas.Keys.Contains("signature")) type = RequestDataType.March;
            else if (datas.Keys.Contains("MsgType"))
            {
                if(datas["MsgType"]=="text")
                    type = RequestDataType.NormalMsg;
                if (datas["MsgType"] == "event")
                {
                    if (datas["Event"] == "subscribe")
                    {
                        type = RequestDataType.Subscribe;
                    }
                    if (datas["Event"] == "unsubscribe")
                    {
                        type = RequestDataType.UnSubscribe;
                    }
                }
            }
            UserManager userManager = new UserManager();
            switch (type)
            {
                case RequestDataType.March:
                    var signature = datas["signature"];
                    var echostr = datas["echostr"];
                    datas.Remove("signature");
                    datas.Remove("echostr");
                    datas.Add("token", General.Token);
                    var dicSort = from objDic in datas orderby objDic.Value ascending select objDic;
                    string ttn = "";
                    foreach(var kv in dicSort)
                    {
                        ttn += kv.Value;
                    }
                    if (SHA1Secure.VerifySha1Hash(ttn, signature))
                    {
                        WriteResponse(echostr);
                    }
                    break;
                case RequestDataType.NormalMsg:
                    var content = datas["Content"];
                    User = UserManager.GetUser(datas["FromUserName"]);
                    User.LastActiveTime = General.GetTimestamp13();
                    switch (User.MsgStatus)
                    {
                        case UserInfo.MessageStatus.Normal:
                            string sendback = MessageHandler.Handle(this, content);
                            if (!SensorFilter.Filter(sendback)&&MessageHandler.Handler is NormalHandler) sendback = "...";
                            WriteResponse(AI.RecievedStringHandler.CreateXmlMessageStructure(User.UserID, sendback));
                            var dialog = new Dialog(User.UserID, content, sendback, User.LastActiveTime);
                            General.mysqlConnection.InsertMessageRecord(dialog);
                            UserManager.SetUser(User);
                            break;
                        case UserInfo.MessageStatus.GettingName:
                            if (content.IndexOf('#') >= 0)
                            {
                                User.Name = content.TrimStart('#');
                                User.MsgStatus = UserInfo.MessageStatus.GettingAge;
                                UserManager.SetUser(User);
                                WriteResponse(AI.RecievedStringHandler.CreateXmlMessageStructure(User.UserID, "hi,"+User.Name+",让小A更了解你！以 #年龄 的格式输入你的年龄。"));
                            }
                            else
                            {
                                WriteResponse(AI.RecievedStringHandler.CreateXmlMessageStructure(User.UserID, "让小A更了解你！以 #姓名 的格式输入你的姓名。"));
                            }
                            break;
                        case UserInfo.MessageStatus.GettingAge:
                            if (content.IndexOf('#') >= 0)
                            {
                                var cvtflag0 = false;
                                int age = 0;
                                cvtflag0 = int.TryParse(content.TrimStart('#'), out age);
                                if (cvtflag0)
                                {
                                    User.Age = age;
                                    User.MsgStatus = UserInfo.MessageStatus.GettingSex;
                                    UserManager.SetUser(User);
                                    WriteResponse(AI.RecievedStringHandler.CreateXmlMessageStructure(User.UserID, "hi," + User.Age + "岁的" + User.Name + ",让小A更了解你！以 #数字 的格式输入你的性别,0代表漂亮妹子(女装大佬),1代表帅气小伙。"));
                                    return;
                                }
                            }
                            WriteResponse(AI.RecievedStringHandler.CreateXmlMessageStructure(User.UserID, "hi," + User.Name + ",让小A更了解你！以 #年龄 的格式输入你的年龄。"));
                            break;
                        case UserInfo.MessageStatus.GettingSex:
                            var cvtflag1 = false;
                            int sex = 0;
                            cvtflag1 = int.TryParse(content.TrimStart('#'), out sex);
                            if (cvtflag1)
                            {
                                if (sex == 0) User.Sexure = UserInfo.Sex.Girl;
                                else if (sex == 1) User.Sexure = UserInfo.Sex.Boy;
                                else
                                {
                                    WriteResponse(AI.RecievedStringHandler.CreateXmlMessageStructure(User.UserID, "#0是女,#1是男，请你认真一点哦(严肃脸)"));
                                    UserManager.SetUser(User);
                                    return;
                                }
                                User.MsgStatus = UserInfo.MessageStatus.Normal;
                                UserManager.SetUser(User);
                                WriteResponse(AI.RecievedStringHandler.CreateXmlMessageStructure(User.UserID, (sex == 0 ? "可爱的" : "帅气的") + User.Age.ToString() + "岁" + (sex == 0 ? "小姐姐" : "小哥哥") + User.Name + ",现在我们可以愉快地聊天啦！"));
                            }
                            WriteResponse(AI.RecievedStringHandler.CreateXmlMessageStructure(User.UserID, "hi," + User.Age + "岁的" + User.Name + ",让小A更了解你！以 #数字 的格式输入你的性别,0代表漂亮妹子(女装大佬),1代表帅气小伙。"));
                            break;
                        case UserInfo.MessageStatus.Study:
                            StudyManager.SetAnswer(User.UserID, content);
                            WriteResponse("success");
                            User.MsgStatus = UserInfo.MessageStatus.Normal;
                            break;
                    }
                    
                    break;
                case RequestDataType.Subscribe:
                    User = UserManager.GetUser(datas["FromUserName"]);
                    WriteResponse(AI.RecievedStringHandler.CreateXmlMessageStructure(User.UserID, "欢迎关注小A!玩得愉快哦😊"));
                    break;
                case RequestDataType.UnSubscribe:
                    WriteResponse("");
                    User = UserManager.GetUser(datas["FromUserName"]);
                    UserManager.DelUser(User);
                    break;
            }
        }
        /// <summary>
        /// 数据类型
        /// </summary>
        enum RequestDataType
        {
            /// <summary>
            /// 未知类型
            /// </summary>
            Unknow,
            /// <summary>
            /// 对接消息
            /// </summary>
            March,
            /// <summary>
            /// 普通消息
            /// </summary>
            NormalMsg,
            /// <summary>
            /// 订阅消息
            /// </summary>
            Subscribe,
            /// <summary>
            /// 取消订阅消息
            /// </summary>
            UnSubscribe,
            /// <summary>
            /// 正在学习
            /// </summary>
        }
    }
}
