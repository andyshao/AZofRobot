﻿using RBTofAZ.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RBTofAZ.Data
{
    class UserManager
    {
        private static List<UserInfo> Users = new List<UserInfo>();
        public static UserInfo GetUser(string user_id)
        {
            var user = Adapt(user_id);
            if (user != null) return user;
            user = General.mysqlConnection.SelectUser(user_id);
            if (user != null)
            {
                Users.Add(user);
                return user;
            }
            user = new UserInfo(user_id);
            Users.Add(user);
            return user;
        }
        public static void SetUser(UserInfo user)
        {
            UserInfo deluser = null;
            foreach(var usr in Users)
            {
                if (usr.UserID == user.UserID)
                {
                    deluser = user;
                    break;
                }
            }
            try
            {
                if (user.MsgStatus == UserInfo.MessageStatus.Normal) General.mysqlConnection.RefreshUser(user);
                Users.Remove(deluser);
                Users.Add(user);
            }
            catch(Exception ex)
            {
                General.Println("SetUser:" + ex.Message);
            }
        }
        public static void DelUser(UserInfo user)
        {
            for(var i = 0; i < Users.Count; i++)
            {
                if (Users[i].UserID == user.UserID)
                {
                    Users.RemoveAt(i);
                    break;
                }
            }
        }
        private static UserInfo Adapt(string id)
        {
            foreach(var user in Users)
            {
                if (user.UserID == id) return user;
            }
            return null;
        }
    }
}
