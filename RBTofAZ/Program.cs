﻿using RBTofAZ.AI.Plugins;
using RBTofAZ.Net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace RBTofAZ
{
    class Program
    {
        static void Main(string[] args)
        {
            General.Println("小A机器人正在启动...");
            PanGu.Segment.Init();
            AI.MessageHandler.Init();
            UI.AdminCmdHandler.Init();
            SensorFilter.Init();
            General.Println("分词服务已启动...");
            int port = 80;
            HttpServer httpServer = new HttpServer(port);
            General.Println("启动http侦听服务...");
            httpServer.Start();
            General.Println("服务启动完成,监听端口:" + port);
            General.Println("输入/frm即可开启可视化后台管理程序.");
            while (true)
            {
                var cmd = Console.ReadLine();
                UI.AdminCmdHandler.Handle(cmd);
            }
        }
    }
}
