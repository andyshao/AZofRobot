﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RBTofAZ.AI.Plugins
{
    class HotWordsHandler : AI.IMessageHandler
    {
        private bool handled = false;
        private static string Get()
        {
            var sb = new StringBuilder("小A为你播报实时热点：\n");
            var url = "http://top.baidu.com";
            var htmlweb = new HtmlWeb();
            htmlweb.OverrideEncoding = Encoding.GetEncoding("gb2312");
            var htmldoc = htmlweb.Load(url);
            for (var i = 1; i <= 10; i++)
            {
                sb.Append(i.ToString() + ".");
                var hnc = htmldoc.DocumentNode.SelectNodes("//*[@id=\"hot-list\"]/li[" + i.ToString() + "]/a[1]");
                var href = hnc[0].GetAttributeValue("href", "");
                href = href.Replace("&", "&amp;");
                sb.Append("&lt;a href=&quot;" + href + "&quot;&gt;" + hnc[0].InnerText + "&lt;/a&gt;" + "\n");
            }
            
            var str = sb.ToString();
            return str;
        }
        string IMessageHandler.Handle(object sender, string msg)
        {
            string sendback = "";
            if (msg.IndexOf("实时热点") == 0)
            {
                handled = true;
                sendback = Get();
            }
            else
            {
                handled = false;
            }
            return sendback;
        }

        bool IMessageHandler.IsHandled()
        {
            return handled;
        }
    }
}
