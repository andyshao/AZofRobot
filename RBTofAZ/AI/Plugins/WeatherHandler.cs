﻿using RBTofAZ.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace RBTofAZ.AI.Plugins
{
    class WeatherHandler:AI.IMessageHandler
    {
        private bool handled = false;
        private static string ReportWeather(string cityName)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                string url = "http://flash.weather.com.cn/wmaps/xml/china.xml";
                WebClient MyWebClient = new WebClient();
                Byte[] pageData = MyWebClient.DownloadData(url);
                string pageHtml = Encoding.UTF8.GetString(pageData);
                XDocument doc = XDocument.Parse(pageHtml);
                //XDocument doc = XDocument.Load(localContent);
                var results = from ele in doc.Descendants("city") where ele.Attribute("cityname").Value.Equals(cityName) select ele;
                foreach (var result in results)
                {
                    sb.Append("小A为你查到了" + cityName + "明日天气");
                    sb.Append("\n天气：" + result.Attribute("stateDetailed").Value);
                    sb.Append("\n气温：" + result.Attribute("tem1").Value + "℃-" + result.Attribute("tem2").Value + "℃");
                    sb.Append("\n风力：" + result.Attribute("windState").Value);
                }
                if (sb.Length == 0) return "小A查不到这个城市的天气呢...(目前仅支持省会城市的查询)";
                return sb.ToString();
            }
            catch (WebException webEx)
            {
                General.Println(webEx.Message.ToString());
                return "天气系统好像出了点问题呢...和小A聊点别的吧";
            }
        }

        string IMessageHandler.Handle(object sender, string msg)
        {
            string sendback = "";
            if (msg.IndexOf("天气") == 0)
            {
                handled = true;
                string[] weather_request = msg.Split(' ');
                if (weather_request.Count() < 2)
                {
                    sendback = "输入“天气 城市”可以查询24小时天气哦！（目前仅支持省会城市查询）";
                }
                else
                {
                    sendback = ReportWeather(weather_request[1]);
                }
            }
            else
            {
                handled = false;
            }
            return sendback;

        }

        bool IMessageHandler.IsHandled()
        {
            return handled;
        }
    }
}
