﻿using RBTofAZ.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RBTofAZ.AI.Plugins
{
    class NormalHandler : AI.IMessageHandler
    {
        string IMessageHandler.Handle(object sender, string msg)
        {
            var sendback = "";
            sendback = AI.RecievedStringHandler.GetSendBackString(msg);
            if (sendback == General.DontUnderstand)
            {
                ((DataHandler)sender).User.MsgStatus = Data.UserInfo.MessageStatus.Study;
                sendback = "小A不知道该怎么回答呢，告诉我这句话的回答吧(●'◡'●)";
                Study.StudyManager.AddDialog(((DataHandler)sender).User.UserID, msg);
            }
            return sendback;
        }

        bool IMessageHandler.IsHandled()
        {
            return true;
        }
    }
}
