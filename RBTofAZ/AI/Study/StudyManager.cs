﻿using RBTofAZ.Net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RBTofAZ.AI.Study
{
    static class StudyManager
    {
        private static List<StudyDialog> Dialogs = new List<StudyDialog>();

        public static void AddDialog(string user_id,string question)
        {
            Dialogs.Add(new StudyDialog(user_id, question));
        }
        public static void SetAnswer(string user_id, string answer)
        {
            foreach(var dialog in Dialogs)
            {
                if (dialog.March(user_id))
                {
                    dialog.SetAnswer(answer);
                }
            }
            foreach(var dialog in Dialogs)
            {
                if (dialog.Finished)
                {
                    General.mysqlConnection.InsertStudyedData(dialog);
                }
            }
            for(int i=0;i<Dialogs.Count;i++)
            {
                if (Dialogs[i].Finished) Dialogs.RemoveAt(i);
                i--;
            }
        }
    }
}
